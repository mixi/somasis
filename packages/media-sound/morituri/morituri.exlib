# Copyright 2013 Ankur Kothari
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="https://github.com/thomasvs/morituri"
SCM_SECONDARY_REPOSITORIES='flog command deps musicbrainzngs'

SCM_flog_REPOSITORY='https://github.com/Flumotion/flog'
SCM_command_REPOSITORY='https://github.com/thomasvs/python-command'
SCM_deps_REPOSITORY='https://github.com/thomasvs/python-deps'
SCM_musicbrainzngs_REPOSITORY='https://github.com/thomasvs/python-musicbrainz-ngs'

ever is_scm || SCM_TAG="v${PV}"

SCM_EXTERNAL_REFS='
    morituri/extern/flog:flog
    morituri/extern/python-command:command
    morituri/extern/python-deps:deps
    morituri/extern/python-musicbrainz-ngs:musicbrainzngs
'

AT_M4DIR=( m4 )

require scm-git
require bash-completion
require python [ blacklist='3' multibuild=false has_bin=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]

HOMEPAGE="https://thomas.apestaart.org/${PN}/trac"

SUMMARY="A CD ripper preferring accuracy over speed"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    mp3
"

DEPENDENCIES="
    build+run:
        app-cdr/cdrdao[>=1.2.3]
        dev-python/gst-python:0.10[python_abis:*(-)?]
        dev-python/pycdio[python_abis:*(-)?] [[ description = [ To find drive offset ] ]]
        dev-python/pyxdg[python_abis:*(-)?]
        dev-python/setuptools[python_abis:*(-)?]
        media/cdparanoia
        media-libs/gstreamer:0.10
        media-plugins/gst-plugins-base:0.10[>=0.10.22]
        media-plugins/gst-plugins-good:0.10[>=0.10.16] [[ description = [ For FLAC and taglib ] ]]
        mp3? ( media-plugins/gst-plugins-ugly:0.10 )
    recommendation:
        dev-python/cddb-py[python_abis:*(-)?] [[ description = [ For showing but not using CDDB info if not in musicbrainz ] ]]
    suggestion:
        media-sound/morituri-whatcd[python_abis:*(-)?] [[ description = [ For What.CD-compatible logging ] ]]
"

REMOTE_IDS="freecode:${PN} github:thomasvs/${PN}"

export_exlib_phases src_prepare src_install

morituri_src_prepare(){
    autotools_src_prepare
}

morituri_src_install() {
    default
    if option bash-completion;then
        edo mkdir -p "${IMAGE}"/usr/share/bash-completion/completions
        edo mv "${IMAGE}"/etc/bash_completion.d/rip "${IMAGE}"/usr/share/bash-completion/completions/
    fi
    edo rm -rf "${IMAGE}"/etc/bash_completion.d
    edo rmdir "${IMAGE}"/etc
}

# needs pychecker
RESTRICT="test"
